import * as React from 'react'
import Interweave, { Matcher, MatchResponse, Node } from 'interweave'

// XXX: curly brackets {} will cause dead-loop.
let content = 'Section 1: $[Name] (SIN: $[SIN])agrees to donate his(her) tissues and organs';

interface CustomProps {
    exp:string;
}


class NameWidget extends React.PureComponent<any, any> {
    render() {
        let placeholder;
        if (this.props.exp === 'Name') { placeholder = 'Your Name'}
        else if (this.props.exp === 'SIN') { placeholder = 'Your SIN'}
        return (
            <input type="text" minLength={1} maxLength={12} size={12} placeholder={placeholder} />
        )
    }
} 

class CustomMatcher extends Matcher<CustomProps> {
    match(string: string):MatchResponse | null {
        let matches = string.match(/\$\[(.*?)\]/)
        if (matches) {
            console.log('>>> ', matches[0])
            return {
                match: matches[0],
                extraProp: '-',
                exp: matches[1]
            }
        }
        else {
            return null
        }
    }


    replaceWith(match: string, props: CustomProps):Node {
        return <span {...props}><NameWidget exp={props.exp} /></span>
    }

    asTag():string {
        return 'span'
    }
}


export default class App extends React.Component<any, any> {

    constructor(props:any) {
        super(props)
        
        this.state = {}
    }



    render() {
        return (
            <div>
                <Interweave
                    content={content}
                    matchers={[new CustomMatcher('tag')]}
                />
            </div>
        )    
    }
}
