import * as React from 'react'
import * as ReactDom from 'react-dom'
import MainPage from './app'

ReactDom.render(
    <MainPage />,
    document.getElementById('react-root')
)
