const path = require('path')
const CopyWebPackPlugin = require('copy-webpack-plugin')

// export configuration
module.exports = {
    mode: 'development',
    // the entry point of your application, like main function in some languages
    // path.resolve makes sure we have an absolute path
    entry: path.resolve(__dirname, 'src/index.tsx'),
    // where to put the build output
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    // rule to process different types of source code files
    module: {
        rules: [          
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'ts-loader' }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader', 
                    {
                        loader: 'css-loader',
                        options: { modules: true }
                    }, 
                    {
                        loader: 'less-loader',
                        options: { paths: [path.resolve(__dirname, '../src')]}
                    }                    
                ]
            },
            {
                test: /\.yaml$/,
                use: [
                    'json-loader',
                    'yaml-loader'
                ]
            }, 
        ]
    },

    resolve: {
        extensions: ['.tsx', '.ts', '.jsx', '.js'],
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'src')
        ]        
    },

    // copy these static resource files to output folder
    plugins: [
        new CopyWebPackPlugin([{from: 'static'}])
    ],
    // generate source-map for better debug experience
    devtool: 'source-map',
    // configure the dev server
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        host: '0.0.0.0',
        port: 8000,
        inline: true,
        index: 'index.html',
        https: false
    }
}